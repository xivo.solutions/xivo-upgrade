# Project xivo-upgrade

This project contains installation of xivo-upgrade component, which handles upgrading of XiVO and MDS. It also handles  
debian distribution upgrades, database migration, etc.

# XiVO upgrade main flow

_Note: Only normal XiVO upgrade is discussed here.  
It does not include MDS upgrade, nor debian distribution upgrades etc._

When a normal XiVO upgrade procedure starts (eg. user runs command `xivo-upgrade`), it is mandatory to **stop all XiVO  
services**. Afterwards the upgrade phase can start. After the upgrade phase is finished, only then we can **start the  
XiVO services once more**.

Specifics of the upgrade phase are in the `real-xivo-upgrade` script.

## XiVO upgrade pre and post steps

If there are any additional steps (scripts) that should not be a part of the upgrade phase, please consider adding them  
into one or more of the folders ending with `.d`. Below is a guide where to add your scripts depending on when do you  
want them to be executed.

- If script should run **before** stopping XiVO services (before upgrade), put it in `pre-stop.d` folder.
- If script should run **after** stopping XiVO services (before upgrade), put it in `post-stop.d` folder.
- If script should run **before** starting XiVO services (after upgrade), put it in `pre-start.d` folder.
- If script should run **after** starting XiVO services (after upgrade), put it in `post-start.d` folder.

## XiVO upgrade script dependencies

Here is a diagram visualising which script files are called with which function during XiVO upgrade. Notice that red  
scripts are not part of this project.

![Script dependencies during normal xivo upgrade](docs/images/normal-xivo-upgrade-script-dependencies.png)